---
kind: Template
apiVersion: v1
metadata:
  annotations:
    openshift.io/display-name: "Redirect Nginx Server"
    tags: "http,nginx,httpd"
    iconClass: icon-load-balancer
    description: A simple Nginx server to redirect trafic to another address
    template.openshift.io/provider-display-name: "CERN"
    template.openshift.io/documentation-url: "https://gitlab.cern.ch/hse-see-co-docker/openshift-nginx-redirect"
    template.openshift.io/support-url: "https://gitlab.cern.ch/hse-see-co-docker/openshift-nginx-redirect/issues"
  name: nginx-redirect
labels:
  template: nginx-redirect
parameters:
  - name: SOURCE_CONTEXT
    value: /old-context
    required: true
  - name: TARGET_URL
    value: http://cern.ch/new-context
    require: true
objects:
  - apiVersion: v1
    kind: ConfigMap
    metadata:
      name: nginx-config
    data:
      nginx.conf: |
        # ### From config map ###
        # This error log will be written regardless of server scope error_log
        # definitions, so we have to set this here in the main scope.
        #
        # Even doing this, Nginx will still try to create the default error file, and
        # log a non-fatal error when it fails. After that things will work, however.
        error_log /nginx/error.log;
        daemon off;
        # The pidfile will be written to /var/run unless this is set.
        pid /nginx/nginx.pid;

        worker_processes 1;

        events {
          worker_connections 1024;
        }

        http {
          # Set an array of temp and cache file options that will otherwise default to
          # restricted locations accessible only to root.
          client_body_temp_path /nginx/client_body;
          fastcgi_temp_path /nginx/fastcgi_temp;
          proxy_temp_path /nginx/proxy_temp;
          scgi_temp_path /nginx/scgi_temp;
          uwsgi_temp_path /nginx/uwsgi_temp;
          rewrite_log on;
          tcp_nopush on;
          tcp_nodelay on;
          keepalive_timeout 65;
          types_hash_max_size 2048;

          include /etc/nginx/mime.types;

          log_format   main '$remote_addr - $remote_user [$time_local] $status '
          '"$request" $body_bytes_sent "$http_referer" '
          '"$http_user_agent" "$http_x_forwarded_for"';

          default_type application/octet-stream;
          include /nginx/server.conf/*;
        }

      nginx.template.conf: |
        server {
          # ### From config map ###
          access_log /nginx/access.log;
          error_log /nginx/error.log;

          #### DEBUG ONLY ###
          #error_log /nginx/error.log;
          #rewrite_log on;

          # IPv4.
          listen 8081;
          server_name    __NAMESPACE__.web.cern.ch;

          # We remove the extra context i.e. /service-sailor
          rewrite ^__SOURCE_CONTEXT__/(.*)$ /$1;
          # redirect to the correct url i.e. http://cern.ch/sailor
          return 301 __TARGET_URL__/$1$is_args$args;
        }
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      labels:
        app: openshift-nginx-redirect
      name: openshift-nginx-redirect
    spec:
      replicas: 1
      selector:
        app: openshift-nginx-redirect
        deploymentconfig: openshift-nginx-redirect
      strategy:
        resources: {}
        rollingParams:
          intervalSeconds: 1
          maxSurge: 25%
          maxUnavailable: 25%
          timeoutSeconds: 600
          updatePeriodSeconds: 1
        type: Rolling
      template:
        metadata:
          labels:
            app: openshift-nginx-redirect
            deploymentconfig: openshift-nginx-redirect
        spec:
          volumes:
            - name: conf
              configMap:
                name: nginx-config
          containers:
          - env:
            - name: "NAMESPACE"
              valueFrom:
               fieldRef:
                apiVersion: v1
                fieldPath: metadata.namespace
            - name: SOURCE_CONTEXT
              value: "${SOURCE_CONTEXT}"
            - name: TARGET_URL
              value: "${TARGET_URL}"
            image: gitlab-registry.cern.ch/hse-see-co-docker/openshift-nginx-redirect
            imagePullPolicy: Always
            name: openshift-nginx-redirect
            ports:
            - containerPort: 8081
              protocol: TCP
            resources: {}
            terminationMessagePath: /dev/termination-log
            volumeMounts:
              - mountPath: /nginx/mapping
                name: conf
          dnsPolicy: ClusterFirst
          restartPolicy: Always
          securityContext: {}
          terminationGracePeriodSeconds: 30
      test: false
      triggers:
      - type: ConfigChange
      - imageChangeParams:
          automatic: true
          containerNames:
          - openshift-nginx-redirect
          from:
            kind: ImageStreamTag
            name: openshift-nginx-redirect:latest
            namespace: service-sailorlight-out
        type: ImageChange
    status: {}
  - apiVersion: v1
    kind: Service
    metadata:
      labels:
        app: openshift-nginx-redirect
      name: openshift-nginx-redirect
    spec:
      ports:
      - name: 8081-tcp
        port: 8081
        protocol: TCP
        targetPort: 8081
      selector:
        deploymentconfig: openshift-nginx-redirect
      sessionAffinity: None
      type: ClusterIP
    status:
      loadBalancer: {}

  - kind: Route
    apiVersion: v1
    metadata:
      name: openshift-nginx-redirect
    spec:
      to:
        kind: Service
        name: openshift-nginx-redirect
      port:
        targetPort: 8081
      tls:
        termination: edge
        insecureEdgeTerminationPolicy: Redirect
