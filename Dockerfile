FROM nginx:stable

MAINTAINER https://gitlab.cern.ch/hse-see-co-docker/openshift-nginx-redirect

LABEL io.k8s.description="A simple Nginx server to redirect trafic to another address" \
      io.k8s.display-name="Redirect Proxy" \
      io.openshift.expose-services="8081:http" \
      io.openshift.tags="http,proxy"

# cleaning default conf
RUN rm /etc/nginx/conf.d/default.conf && rm /etc/nginx/nginx.conf && \
    #Preparing tmp dir,
    mkdir -p /nginx/server.conf

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /nginx/access.log
RUN ln -sf /dev/stderr /nginx/error.log

ADD startNginx.sh /nginx/

RUN chmod -R 777 /nginx && \
    chmod 777 /nginx/startNginx.sh

EXPOSE 8081

USER 1001

CMD ["/nginx/startNginx.sh"]