#!/bin/sh

cd /nginx
cat /nginx/mapping/nginx.template.conf | \
    sed -e "s%__NAMESPACE__%${NAMESPACE}%g" |\
    sed -e "s%__TARGET_URL__%${TARGET_URL}%g" |\
    sed -e "s%__SOURCE_CONTEXT__%${SOURCE_CONTEXT}%g" \
    > server.conf/redirect-server.conf

OUT=$?

if [ $OUT -eq 0 ];then
    echo "Starting Nginx, redirecting ${NAMESPACE}.web.cern.ch${SOURCE_CONTEXT} to ${TARGET_URL} using :"
    cat /nginx/server.conf/redirect-server.conf
   nginx -c /nginx/mapping/nginx.conf
else
   echo "Invalid configuration file"
   exit $OUT
fi

